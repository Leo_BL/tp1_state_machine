namespace Game
{
    public static class Tags
    {
        public const string MAIN_CONTROLLER = "MainController";
        public const string NAVIGATION_MESH = "NavigationMesh";
        public const string TERRAIN = "Terrain";
        public const string FLORA = "Flora";
        public const string WATER = "Water";
    }
}