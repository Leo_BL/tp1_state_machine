﻿using System.Data.Common;

namespace Game
{
    /// <summary>
    /// Benjamin Lemelin & Léo Bélanger-Lagacé
    /// </summary>
    public static class DbExtensions
    {
        public static void CreateParameter(this DbCommand command, object value)
        {
            var parameter = command.CreateParameter();
            parameter.Value = value;
            command.Parameters.Add(parameter);
        }
    }
}