﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Léo Bélanger-Lagacé
    /// </summary>
    public class SimulationRepository : Repository<Simulation>
    {
        public override void Create(Simulation simulationItem)
        {
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = SimulationQueries.CREATE;
                    command.CreateParameter(simulationItem.Seed);
                    
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                RollbackTransaction();
                CloseConnection();
                Debug.Log(e);
                throw;
            }
        }

        public override Simulation ReadOne(int id)
        {
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = SimulationQueries.READ_ONE;
                    command.CreateParameter(id);
                    return ReadSimulations(command)[0];
                }
            }
            catch (Exception e)
            {
                RollbackTransaction();
                CloseConnection();
                Debug.Log(e);
                throw;
            }
        }

        public override List<Simulation> ReadAll()
        {
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = SimulationQueries.READ_ALL;
                    return ReadSimulations(command);
                }
            }
            catch (Exception e)
            {
                RollbackTransaction();
                CloseConnection();
                Debug.Log(e);
                throw;
            }
        }

        public override void Update(Simulation simulationItem)
        {
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = SimulationQueries.UPDATE;
                    command.CreateParameter(simulationItem.Seed);
                    command.CreateParameter(simulationItem.IdSim);
                    
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                RollbackTransaction();
                CloseConnection();
                Debug.Log(e);
                throw;
            }
        }

        public override void Delete(Simulation simulationItem)
        {
            if (simulationItem.IdSim.HasValue)
                Delete(simulationItem.IdSim.Value);
        }

        public override void Delete(int id)
        {
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = SimulationQueries.DELETE;
                    command.CreateParameter(id);
                    
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                RollbackTransaction();
                CloseConnection();
                Debug.Log(e);
                throw;
            }
        }
        
        /// <summary>
        /// Hugues Beaupré-Touchette
        /// </summary>
        private List<Simulation> ReadSimulations(DbCommand command)
        {
            var reader = command.ExecuteReader();
            var listSim = new List<Simulation>();
            while (reader.Read())
            {
                listSim.Add(new Simulation
                (
                    Convert.ToInt32(reader["ID_sim"]),
                    Convert.ToInt32(reader["seed"])
                ));
            }

            return listSim;
        }
    }
}
