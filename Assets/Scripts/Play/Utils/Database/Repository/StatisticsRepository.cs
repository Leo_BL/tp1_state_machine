﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Hugues Beaupré-Touchette
    /// </summary>
    public class StatisticsRepository : Repository<Statistics>
    {
        public override void Create(Statistics statisticsItem)
        {
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = StatisticsQueries.CREATE;
                    command.CreateParameter(statisticsItem.BunnyDehytrated);
                    command.CreateParameter(statisticsItem.BunnyStarved);
                    command.CreateParameter(statisticsItem.BunnyEaten);
                    command.CreateParameter(statisticsItem.BunnyDeath);
                    command.CreateParameter(statisticsItem.FoxDehydrated);
                    command.CreateParameter(statisticsItem.FoxStarved);
                    command.CreateParameter(statisticsItem.FoxDeath);
                    command.CreateParameter(statisticsItem.BunnyPop);
                    command.CreateParameter(statisticsItem.FoxPop);
                    command.CreateParameter(statisticsItem.IdSim);
                    command.CreateParameter(statisticsItem.PresentTime);
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                RollbackTransaction();
                CloseConnection();
                Debug.Log(e);
                throw;
            }
            
        }

        public override Statistics ReadOne(int id)
        {
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = StatisticsQueries.READ_ONE;
                    command.CreateParameter(id.ToString());
                    return ReadStatistics(command).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                RollbackTransaction();
                CloseConnection();
                Debug.Log(e);
                throw;
            }
        }

        public override List<Statistics> ReadAll()
        {
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = StatisticsQueries.READ_ALL;
                    return ReadStatistics(command);
                }
            }
            catch (Exception e)
            {
                RollbackTransaction();
                CloseConnection();
                Debug.Log(e);
                throw;
            }
        }

        public override void Update(Statistics statisticsItem)
        {
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = StatisticsQueries.UPDATE;
                    command.CreateParameter(statisticsItem.BunnyDehytrated);
                    command.CreateParameter(statisticsItem.BunnyStarved);
                    command.CreateParameter(statisticsItem.BunnyEaten);
                    command.CreateParameter(statisticsItem.BunnyDeath);
                    command.CreateParameter(statisticsItem.FoxDehydrated);
                    command.CreateParameter(statisticsItem.FoxStarved);
                    command.CreateParameter(statisticsItem.FoxDeath);
                    command.CreateParameter(statisticsItem.BunnyPop);
                    command.CreateParameter(statisticsItem.FoxPop);
                    command.CreateParameter(statisticsItem.IdSim);
                    command.CreateParameter(statisticsItem.PresentTime);
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                RollbackTransaction();
                CloseConnection();
                Debug.Log(e);
                throw;
            }
        }

        public override void Delete(Statistics statisticsItem)
        {
            if (statisticsItem.IdStats.HasValue)
                Delete((int)statisticsItem.IdStats);
        }

        public override void Delete(int id)
        {
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = StatisticsQueries.DELETE;
                    command.CreateParameter(id.ToString());
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                RollbackTransaction();
                CloseConnection();
                Console.WriteLine(e);
                throw;
            }

        }
        
        private List<Statistics> ReadStatistics(DbCommand command)
        {
            var reader = command.ExecuteReader();
            var list = new List<Statistics>();
            while (reader.Read())
                list.Add(new Statistics
                (
                    Convert.ToInt32(reader["ID_stats"]),
                    Convert.ToInt32(reader["bunny_dehydrated"]),
                    Convert.ToInt32(reader["bunny_starved"]),
                    Convert.ToInt32(reader["bunny_eaten"]),
                    Convert.ToInt32(reader["bunny_death"]),
                    Convert.ToInt32(reader["fox_dehydrated"]),
                    Convert.ToInt32(reader["fox_starved"]),
                    Convert.ToInt32(reader["fox_death"]),
                    Convert.ToInt32(reader["bunny_pop"]),
                    Convert.ToInt32(reader["fox_pop"]),
                    Convert.ToInt32(reader["ID_sim"]),
                    Convert.ToInt32(reader["present_time"])
                ));
            return list;
        }
        
    }
}