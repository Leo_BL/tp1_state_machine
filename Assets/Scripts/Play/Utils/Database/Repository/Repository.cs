﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using UnityEngine;

namespace Game
{
    public abstract class Repository
    {
        /// <summary>
        /// Léo Bélanger-Lagacé & Hugues Beaupré-Touchette
        /// </summary>
        
        //BEN_APPROVED : JADE ROCKS
        protected static DbConnection connection;
        protected static DbTransaction transaction;
        
        public static void OpenConnection()
        {
            if (connection == null)
            {
                connection = null;
                transaction = null;
                try
                {
                    connection = Finder.SqLiteConnectionFactory.GetConnection();
                    connection.Open();
                    BeginTransaction();
                }
                catch (Exception e)
                {
                    RollbackTransaction();
                    CloseConnection();
                    Debug.Log(e);
                }
            }
            
        }

        protected static void BeginTransaction()
        {
            if(transaction == null)
                transaction = connection?.BeginTransaction();
        }

        protected static void RollbackTransaction()
        {
            transaction?.Rollback();
            transaction = null;
        }

        protected static void CommitTransaction()
        {
            transaction?.Commit();
            transaction = null;
        }
        
        public static void CloseConnection()
        {
            if (connection != null)
            {
                CommitTransaction();
                
                connection.Close();
                connection.Dispose();
                connection = null;
            }
        }
    }
    
    public abstract class Repository<T> : Repository
    {
        public const string QUERY_GET_ID = "SELECT last_insert_rowid();";
        
        public abstract void Create(T item);
        public abstract T ReadOne(int id);
        public abstract List<T> ReadAll();
        public abstract void Update(T item);
        public abstract void Delete(T item);
        public abstract void Delete(int id);

        public int GetId()
        {
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = QUERY_GET_ID;
                    
                    return Convert.ToInt32(command.ExecuteScalar());
                }
            }
            catch (Exception e)
            {
                Debug.Log(e);
                RollbackTransaction();
                throw;
            }
        }
    }
}