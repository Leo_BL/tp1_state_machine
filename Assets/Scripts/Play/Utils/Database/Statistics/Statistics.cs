﻿using Boo.Lang;

namespace Game
{
    /// <summary>
    /// Hugues Beaupré-Touchette & Léo Bélanger-Lagacé
    /// </summary>
    public class Statistics
    {
        public int? IdStats { get; set; }
        public int BunnyDehytrated { get; set; }
        public int BunnyStarved { get; set; }
        public int BunnyEaten { get; set; }
        public int FoxDehydrated { get; set; }
        public int FoxStarved { get; set; }
        public int BunnyDeath { get; set; }
        public int FoxDeath { get; set; }
        public int BunnyPop { get; set; }
        public int FoxPop { get; set; }
        public int IdSim { get; set; }
        public int PresentTime { get; set; }

        public Statistics()
            : this(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
        {
        }

        public Statistics(int bunnyDehydrated, int bunnyStarved, int bunnyEaten, int bunnyDeath, int foxDehydrated,
            int foxStarved, int foxDeath, int bunnyPop, int foxPop, int idsim, int presentTime)
            : this(null, bunnyDehydrated, bunnyStarved, bunnyEaten, bunnyDeath, foxDehydrated, foxStarved, foxDeath,
                bunnyPop, foxPop, idsim, presentTime)
        {
        }

        public Statistics(int? idstats, int bunnyDehydrated, int bunnyStarved, int bunnyEaten, int bunnyDeath,
            int foxDehydrated, int foxStarved, int foxDeath, int bunnyPop, int foxPop, int idsim, int presentTime)
        {
            this.IdStats = idstats;
            this.BunnyDehytrated = bunnyDehydrated;
            this.BunnyStarved = bunnyStarved;
            this.BunnyEaten = bunnyEaten;
            this.BunnyDeath = bunnyDeath;
            this.FoxDehydrated = foxDehydrated;
            this.FoxStarved = foxStarved;
            this.FoxDeath = foxDeath;
            this.BunnyPop = bunnyPop;
            this.FoxPop = foxPop;
            this.IdSim = idsim;
            this.PresentTime = presentTime;
        }

        public override string ToString()
        {
            return "Statistics object => IdStats : " + IdStats + " BunnyDehytrated : " + BunnyDehytrated +
                   " BunnyStarved : " + BunnyStarved + " BunnyEaten : " + BunnyEaten +
                   " BunnyDeath : " + BunnyDeath + " FoxDehydrated : " + FoxDehydrated +
                   " FoxStarved : " + FoxStarved + " FoxDeath : " + FoxDeath + " BunnyPop : " + BunnyPop +
                   " FoxPop : " + FoxPop
                   + " IdSim : " + IdSim + " PresentTime : " + PresentTime;
        }

        public override bool Equals(object obj)
        {
            if (this == obj)
                return true;
            if (obj == null || obj.GetType() != GetType())
                return false;

            Statistics stats = (Statistics) obj;
            return IdStats == stats.IdStats && BunnyDehytrated == stats.BunnyDehytrated &&
                   BunnyStarved == stats.BunnyStarved && BunnyEaten == stats.BunnyEaten &&
                   FoxDehydrated == stats.FoxDehydrated && FoxStarved == stats.FoxStarved &&
                   BunnyDeath == stats.BunnyDeath && FoxDeath == stats.FoxDeath &&
                   BunnyPop == stats.BunnyPop && FoxPop == stats.FoxPop && IdSim == stats.IdSim &&
                   PresentTime == stats.PresentTime;
        }
    }
}