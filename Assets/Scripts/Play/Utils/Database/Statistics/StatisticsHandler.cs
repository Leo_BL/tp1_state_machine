﻿
using System.Collections;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Léo Bélanger-Lagacé & Hugues Beaupré-Touchette
    /// </summary>
    public class StatisticsHandler : MonoBehaviour
    {
        private StatisticsRepository statisticsRepository;
        private Statistics statistics;

        private void Awake()
        {
            statisticsRepository = new StatisticsRepository();
            statistics = new Statistics();
        }
        
        private void OnEnable()
        {
            Repository.OpenConnection();
        }

        private void Start()
        {
            StartCoroutine(InsertStatisticsRoutine());
        }
        
        private void OnDisable()
        {
            Repository.CloseConnection();
        }

        public void IncrementDeath(Animal animal)
        {
            if (animal is Bunny)
            {
                statistics.BunnyDeath++;
                statistics.BunnyPop--;
                switch (animal.DeathCause)
                {
                    case DeathCause.THIRST:
                        statistics.BunnyDehytrated++;
                        break;
                    case DeathCause.HUNGER:
                        statistics.BunnyStarved++;
                        break;
                    case DeathCause.EATEN:
                        statistics.BunnyEaten++;
                        break;
                }
            }
            else
            {
                statistics.FoxDeath++;
                statistics.FoxPop--;
                switch (animal.DeathCause)
                {
                    case DeathCause.THIRST:
                        statistics.FoxDehydrated++;
                        break;
                    case DeathCause.HUNGER:
                        statistics.FoxStarved++;
                        break;
                }
            }
        }

        public void IncrementBirth(Animal animal)
        {
            if (animal is Bunny)
                statistics.BunnyPop++;
            else
                statistics.FoxPop++;
        }
        
        private IEnumerator InsertStatisticsRoutine()
        {
            while (isActiveAndEnabled)
            {
                yield return new WaitForSeconds(1);
                
                statisticsRepository.Create(statistics);
            }
        }
    }
}