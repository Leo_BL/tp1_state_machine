﻿namespace Game
{
    /// <summary>
    /// Léo Bélanger-Lagacé
    /// </summary>
    public class StatisticsQueries
    {
        public const string CREATE = "INSERT INTO statistics(bunny_dehydrated, bunny_starved, bunny_eaten, bunny_death, fox_dehydrated, fox_starved, fox_death, bunny_pop, fox_pop, ID_sim, present_time) VALUES (?,?,?,?,?,?,?,?,?,?,?);";

        public const string READ_ONE = "SELECT * FROM statistics WHERE ID_stats = ?;";

        public const string READ_ALL = "SELECT * FROM statistics;";

        public const string UPDATE = "UPDATE statistics SET bunny_dehydrated = ?, bunny_starved = ?, bunny_eaten = ?, bunny_death = ?, fox_dehydrated = ?, fox_starved = ?, fox_death = ?, bunny_pop = ?, fox_pop = ?, ID_sim = ?, present_time = ? WHERE ID_death = ?;";

        public const string DELETE = "DELETE FROM statistics WHERE ID_stats = ?;";
    }
}