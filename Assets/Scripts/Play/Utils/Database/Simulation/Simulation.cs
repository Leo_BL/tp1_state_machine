﻿namespace Game
{
    /// <summary>
    /// Léo Bélanger-Lagacé
    /// </summary>
    public class Simulation
    {
        public int? IdSim { get; set; }
        public int Seed { get; set; }

        public Simulation(int seed)
            :this(null, seed)
        {
        }
        
        public Simulation(int? idsim, int seed)
        {
            IdSim = idsim;
            Seed = seed;
        }
        
        public override bool Equals(object obj)
        {
            if (this == obj)
                return true;
            if (obj == null || obj.GetType() != GetType())
                return false;
            
            Simulation simulation = (Simulation) obj;
            return IdSim == simulation.IdSim && Seed == simulation.Seed;
        }

        public override string ToString()
        {
            return "\nSimulation object: Idsim: " + IdSim + " seed: " + Seed;
        }
    }
}