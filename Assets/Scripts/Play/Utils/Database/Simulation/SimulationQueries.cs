﻿namespace Game
{
    /// <summary>
    /// Léo Bélanger-Lagacé
    /// </summary>
    public class SimulationQueries
    {
        public const string CREATE = "INSERT INTO sim(seed) VALUES (?);";

        public const string READ_ONE = "SELECT * FROM sim WHERE ID_sim = ?;";

        public const string READ_ALL = "SELECT * FROM sim;";

        public const string UPDATE = "UPDATE sim SET seed = ? WHERE ID_sim = ?;";

        public const string DELETE = "DELETE FROM sim WHERE ID_sim = ?;";

    }
}