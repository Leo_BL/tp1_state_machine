﻿using System;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Léo Bélanger-Lagacé
    /// </summary>
    public class SimulationHandler : MonoBehaviour
    {
        private SimulationRepository simulationRepository;
        private Simulation simulation;

        private void Awake()
        {
            simulationRepository = new SimulationRepository();
            simulation = new Simulation(Finder.RandomSeed.Seed);
        }
        
        private void OnEnable()
        {
            Repository.OpenConnection();
        }

        private void Start()
        {
            simulationRepository.Create(simulation);
            simulation.IdSim = simulationRepository.GetId();
        }

        private void OnDisable()
        {
            Repository.CloseConnection();
        }
    }
}