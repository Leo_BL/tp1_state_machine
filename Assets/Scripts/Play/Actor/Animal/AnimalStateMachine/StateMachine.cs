﻿using UnityEngine.Experimental.PlayerLoop;

namespace Game
{
    public class StateMachine
    {
        private State currentState;
        private StateChanger stateChanger;
        public bool IsDisturbable { get => currentState is DisturbableState;}

        public StateMachine(Animal animal)
        {
            stateChanger = new StateChanger(animal);
            currentState = new WanderState(animal, stateChanger);
            stateChanger.SetAllStates
            (
                new FoundPredatorState(animal, stateChanger), 
                new WaitingForMateState(animal, stateChanger),
                new HungryState(new FoundFoodState(animal, stateChanger), animal, stateChanger),
                new ThirstyState(new FoundWaterState(animal, stateChanger), animal, stateChanger),
                new HornyState(new FoundMateState(animal, stateChanger), animal, stateChanger),
                currentState as WanderState
            );
        }

        public void Update()
        {
            if (currentState != null)
                currentState = currentState.Update();
        }

        public void SetWaitingForMate(IEntity target)
        {
            stateChanger.SetWaitingForMateTarget(target);
        }
    }
}