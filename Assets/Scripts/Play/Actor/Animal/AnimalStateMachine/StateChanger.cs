﻿using System;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Hugues Beaupré-Touchette & Léo Bélanger-Lagacé
    /// </summary>
    public class StateChanger
    {
        private readonly Animal animal;
        
        private FoundPredatorState foundPredatorState;
        private WaitingForMateState waitingForMateState;
        private HungryState hungryState;
        private ThirstyState thirstyState;
        private HornyState hornyState;
        private WanderState wanderState;
        private bool allStatesAreInitialised = false;
        
        public StateChanger(Animal animal)
        {
            this.animal = animal;
        }

        public void SetAllStates(FoundPredatorState foundPredatorState, WaitingForMateState waitingForMateState, HungryState hungryState,
                                ThirstyState thirstyState, HornyState hornyState, WanderState wanderState)
        {
            if (!allStatesAreInitialised)
            {
                if (foundPredatorState == null)
                    throw new ArgumentNullException();
                this.foundPredatorState = foundPredatorState;
                if (waitingForMateState == null)
                    throw new ArgumentNullException();
                this.waitingForMateState = waitingForMateState;
                if (hungryState == null)
                    throw new ArgumentNullException();
                this.hungryState = hungryState;
                if (thirstyState == null)
                    throw new ArgumentNullException();
                this.thirstyState = thirstyState;
                if (hornyState == null)
                    throw new ArgumentNullException();
                this.hornyState = hornyState;
                if (wanderState == null)
                    throw new ArgumentNullException();
                this.wanderState = wanderState;
                allStatesAreInitialised = true;
            }
        }


        public State Update()
        {
            if (!allStatesAreInitialised)
                throw new NullReferenceException();
            if (animal.IsWaitingForMate)
            {
                return waitingForMateState;
            }
            IPredator predator = animal.GetPredatorInRange();
            if (predator != null)
            {
                foundPredatorState.SetTarget(predator as Animal);
                return foundPredatorState;
            }
            if (animal.IsThirsty)
            {
                return thirstyState;
            }
            if (animal.IsHungry)
            {
                return hungryState;
            }
            if (animal.IsHorny)
            {
                return hornyState;
            }

            return wanderState;
        }

        public void SetWaitingForMateTarget(IEntity mate)
        {
            waitingForMateState.SetTarget(mate);
        }
    }
}