﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Hugues Beaupré-Touchette & Léo Bélanger-Lagacer
    /// </summary>
    public abstract class State
    {

        protected Animal animal;
        protected StateChanger stateChanger;
        
        protected State(Animal animal, StateChanger stateChanger)
        {
            this.animal = animal;
            this.stateChanger = stateChanger;
        }

        protected abstract void SetPath();

        public abstract State Update();
    }
}