﻿using System.Collections.Generic;
using Harmony;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;

namespace Game
{    
    /// <summary>
    /// Author : Hugues Beaupré-Touchette
    /// </summary>
    public abstract class UndisturbableState : State
    {
        //BEN_APPROVED : JADE
        protected const float TARGET_DISTANCE_MARGIN = .5f;
        protected IEntity target;
        private bool foundTarget = false;

        protected Vector3 TargetPosition => target.Position;

        protected Vector3 AnimalPosition => animal.Position;

        protected UndisturbableState(Animal animal, StateChanger stateChanger)
            : base(animal, stateChanger)
        {
        }

        public void SetTarget(IEntity target)
        {
            animal.ResetPath();
            this.target = target;
            foundTarget = false;
        }
        
        public override State Update()
        {
            if (foundTarget)
            {
                return stateChanger.Update();
            }
            else
            {
                SetPath();
                animal.Move();
                if (GoalIsAchieved())
                    PlayActionToTarget(target);
                return this;
            }
        }

        protected override void SetPath()
        {
            animal.FindPathTo(TargetPosition);
        }

        protected virtual bool GoalIsAchieved()
        {
            return AnimalPosition.SqrDistanceTo(TargetPosition) < TARGET_DISTANCE_MARGIN;
        }


        protected virtual void PlayActionToTarget(IEntity target)
        {
            foundTarget = true;
        }
    }
}