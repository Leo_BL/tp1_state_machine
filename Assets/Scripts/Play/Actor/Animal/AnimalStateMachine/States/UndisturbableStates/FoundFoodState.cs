﻿using Game;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Author : Hugues Beaupré-Touchette
    /// </summary>
    public class FoundFoodState : UndisturbableState
    {
        public FoundFoodState(Animal animal, StateChanger stateChanger)
            : base(animal, stateChanger)
        {
            
        }

        public override State Update()
        {
            if (!(target as IEatable).IsEatable)
                return stateChanger.Update();

            return base.Update();
        }

        protected override void PlayActionToTarget(IEntity target)
        {
            base.PlayActionToTarget(target);
            animal.Eat(target as IEatable);
        }
    }
}