﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Author : Hugues Beaupré-Touchette
    /// </summary>
    public class WaitingForMateState : UndisturbableState
    {
        public WaitingForMateState(Animal animal, StateChanger stateChanger)
            : base(animal, stateChanger)
        {
        }

        public override State Update()
        {
            if ((target as Animal).IsDead)
                return stateChanger.Update();

            return base.Update();
        }

        protected override void SetPath()
        {
            animal.ResetPath();
        }

        protected override void PlayActionToTarget(IEntity target)
        {
            base.PlayActionToTarget(target);
            animal.Mate();
        }
        
    }
}