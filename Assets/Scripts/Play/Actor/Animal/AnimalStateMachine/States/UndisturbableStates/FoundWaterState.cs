﻿using Game;

namespace Game
{
    /// <summary>
    /// Author : Hugues Beaupré-Touchette
    /// </summary>
    public class FoundWaterState : UndisturbableState
    {
        public FoundWaterState(Animal animal, StateChanger stateChanger)
            : base(animal, stateChanger)
        {
        }

        public override State Update()
        {
            animal.FindPathTo(TargetPosition);
            return base.Update();
        }

        protected override void PlayActionToTarget(IEntity target)
        {
            base.PlayActionToTarget(target);
            animal.Drink(target as IDrinkable);
        }
    }
}