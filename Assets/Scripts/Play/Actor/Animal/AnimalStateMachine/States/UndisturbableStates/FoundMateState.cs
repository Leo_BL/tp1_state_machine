﻿namespace Game
{
    /// <summary>
    /// Author : Hugues Beaupré-Touchette
    /// </summary>
    public class FoundMateState : UndisturbableState
    {
        public FoundMateState(Animal animal, StateChanger stateChanger)
            : base(animal, stateChanger)
        {
        }
        
        public override State Update()
        {
            if ((target as Animal).IsDead)
                return stateChanger.Update();

            return base.Update();
        }

        protected override void PlayActionToTarget(IEntity target)
        {
            base.PlayActionToTarget(target);
            animal.CreateLife(target as Animal);
        }

    }
}