﻿
using System.Collections.Generic;

namespace Game
{
    /// <summary>
    /// Author : Hugues Beaupré-Touchette
    /// </summary>
    public class FoundPredatorState : UndisturbableState
    {
    
        private const float PREDATOR_DISTANCE_MARGIN = 4f;

        
        public FoundPredatorState(Animal animal, StateChanger stateChanger) : base(animal, stateChanger)
        {
            
        }

        public override State Update()
        {
            if ((target as Animal).IsDead)
                return stateChanger.Update();

            return base.Update();
        }

        protected override void SetPath()
        {
            animal.FindFleePath(TargetPosition);
        }

        protected override bool GoalIsAchieved()
        {
            return (AnimalPosition - TargetPosition).sqrMagnitude > PREDATOR_DISTANCE_MARGIN;
        }


    }
}