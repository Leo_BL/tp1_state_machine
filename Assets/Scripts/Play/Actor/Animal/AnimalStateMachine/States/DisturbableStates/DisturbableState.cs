﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Author : Léo Bélanger-Lagacé
    /// </summary>
    public abstract class DisturbableState : State
    {
        protected UndisturbableState undisturbableState;
        
        protected DisturbableState(UndisturbableState undisturbableState, Animal animal, StateChanger stateChanger)
            :base(animal, stateChanger)
        {
            this.undisturbableState = undisturbableState;
        }

        protected override void SetPath()
        {
            animal.FindRandomPath();
        }

        public override State Update()
        {
            SetPath();
            animal.Move();
            return stateChanger.Update();
        }
        
        
    }
}