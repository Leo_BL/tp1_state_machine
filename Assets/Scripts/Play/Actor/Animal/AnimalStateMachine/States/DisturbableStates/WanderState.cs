﻿using System.Collections.Generic;
using Game;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Author : Léo Bélanger-Lagacé
    /// </summary>
    public class WanderState : DisturbableState
    {
        public WanderState(Animal animal, StateChanger stateChanger) 
            : base(null, animal, stateChanger)
        {
        }
    }
}