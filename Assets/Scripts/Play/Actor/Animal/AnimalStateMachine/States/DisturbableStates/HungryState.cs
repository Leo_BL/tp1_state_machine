﻿using System.Collections.Generic;
using Game;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Author : Léo Bélanger-Lagacé
    /// </summary>
    public class HungryState : DisturbableState
    {
        public HungryState(FoundFoodState undisturbableState, Animal animal, StateChanger stateChanger) 
            : base(undisturbableState, animal, stateChanger)
        {
        }
        public override State Update()
        {
            if (animal.GetFoodInRange() != null)
            {
                undisturbableState.SetTarget(animal.GetFoodInRange());
                
                return undisturbableState;
            }

            return base.Update();
        } 
    }
}