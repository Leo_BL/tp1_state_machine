﻿using System.Collections.Generic;
using Game;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Author : Léo Bélanger-Lagacé
    /// </summary>
    public class HornyState : DisturbableState
    {
        public HornyState(FoundMateState undisturbableState, Animal animal, StateChanger stateChanger) 
            : base(undisturbableState, animal, stateChanger)
        {
        }
        
        public override State Update()
        {
            IEntity target;
            if ((target = animal.GetMateInRange()) != null)
            {
                (target as Animal).GetReadyToReceiveMate(animal);
                undisturbableState.SetTarget(target);
                
                return undisturbableState;
            }

            return base.Update();
        }
        
    }
}