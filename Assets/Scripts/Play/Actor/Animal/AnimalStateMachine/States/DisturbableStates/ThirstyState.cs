﻿using System.Collections.Generic;
using Game;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Author : Léo Bélanger-Lagacé
    /// </summary>
    public class ThirstyState : DisturbableState
    {
        public ThirstyState(FoundWaterState undisturbableState, Animal animal, StateChanger stateChanger) 
            : base(undisturbableState, animal, stateChanger)
        {
        }
        
        public override State Update()
        {
            var waterInRange = animal.GetWaterInRange();
            if (waterInRange != null)
            {
                undisturbableState.SetTarget(waterInRange);
                
                return undisturbableState;
            }

            return base.Update();
        }
    }
}