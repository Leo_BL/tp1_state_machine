using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public abstract class Animal : Actor
    {
        [Header("Vitals")] [SerializeField] private float hungerThreshold = 0.8f;
        [SerializeField] private float thirstThreshold = 0.8f;
        [SerializeField] private float reproductiveUrgeThreshold = 0.8f;

        private VitalStats vitals;
        private Mover mover;
        private PathFinder pathFinder;
        private Feeder feeder;
        private OffspringCreator offspringCreator;
        private Sensor sensor;
        private List<Node> path;
        private bool isWaitingForMate;

        private DeathCause deathCause;
        
        private StateMachine stateMachine;

        public VitalStats Vitals => vitals;
        protected Mover Mover => mover;
        protected Feeder Feeder => feeder;
        protected OffspringCreator OffspringCreator => offspringCreator;
        protected Sensor Sensor => sensor;

        public bool IsHungry => vitals.Hunger > hungerThreshold;
        public bool IsThirsty => vitals.Thirst > thirstThreshold;
        public bool IsHorny => vitals.ReproductiveUrge > reproductiveUrgeThreshold;
        public bool IsDead => vitals.IsDead;

        protected IEatable targetFood;
        public IEatable TargetFood => targetFood;

        protected IDrinkable targetWater;
        public IDrinkable TargetWater => targetWater;

        protected IEntity targetMate;
        public IEntity TargetMate => targetMate;

        public bool IsWaitingForMate
        {
            get => isWaitingForMate;
            set => value = isWaitingForMate;
        }

        public DeathCause DeathCause
        {
            get => deathCause;
        }

        public bool IsMoving
        {
            get => Mover.IsMoving;
        }

        public bool IsDisturbable
        {
            get => !(IsDead || isWaitingForMate || !stateMachine.IsDisturbable); 
        }


        protected void Awake()
        {
            vitals = GetComponentInChildren<VitalStats>();
            mover = GetComponentInChildren<Mover>();
            feeder = GetComponentInChildren<Feeder>();
            offspringCreator = GetComponentInChildren<OffspringCreator>();
            sensor = GetComponentInChildren<Sensor>();
            pathFinder = Finder.PathFinder;
        }

        protected void Start()
        {
            stateMachine = new StateMachine(this);
            deathCause = DeathCause.EATEN;
            Finder.AnimalBornEventChannel.Publish(this);
        }

        protected void Update()
        {
#if UNITY_EDITOR
            try
            {
#endif
                stateMachine.Update();
#if UNITY_EDITOR
            }
            catch (Exception ex)
            {
                Debug.LogError($"{name} errored : {ex.Message}\n{ex.StackTrace}.", gameObject);
                gameObject.SetActive(false);
            }
#endif
        }

        public void FindRandomPath()
        {
            if (!IsMoving && (path == null || path.Count <= 0))
                path = Finder.PathFinder.FindRandomWalk(Position, 10);
        }
        
        public void FindPathTo(Vector3 targetPosition)
        {
            if (!IsMoving && (path == null || path.Count <= 0))
                path = pathFinder.FindPath(Position, targetPosition);
        }

        public void FindFleePath(Vector3 predatorPosition)
        {
            if (!IsMoving && (path == null || path.Count <= 0))
            {
                var nextNode = pathFinder.FindFleePath(Position, predatorPosition);
                path = nextNode == null ? null : new List<Node>() {nextNode};
            }
        }

        public void Move()
        {
            if (!IsMoving)
            {
                if (path != null && path.Count > 0)
                {
                    Mover.MoveTo(path[0].Position3D);
                    path.RemoveAt(0);
                }
            }
        }

        public void ResetPath()
        {
            path = null;
        }

        private void OnEnable()
        {
            vitals.OnDeath += OnDeath;

            if (vitals.IsDead) OnDeath();
        }

        private void OnDisable()
        {
            vitals.OnDeath -= OnDeath;
        }

        private void OnDeath()
        {
            if (vitals.Thirst >= vitals.ThirstDeathThreshold)
                deathCause = DeathCause.THIRST;

            else if (vitals.Hunger >= vitals.HungerDeathThreshold)
                deathCause = DeathCause.HUNGER;

            Finder.AnimalDeathEventChannel.Publish(this);
            Destroy();
        }

        [ContextMenu("Destroy")]
        private void Destroy()
        {
            Destroy(gameObject);
        }

        public void Eat(IEatable food)
        {
            Feeder.Eat(food);
        }
        
        public void Drink(IDrinkable water)
        {
            Feeder.Drink(water);
        }

        public abstract IPredator GetPredatorInRange();

        public abstract IEatable GetFoodInRange();
        public abstract IEntity GetMateInRange();

        public IDrinkable GetWaterInRange()
        {
            if (Sensor.For<IDrinkable>().SensedObjects.Count > 0)
                return Sensor.For<IDrinkable>().SensedObjects[0];

            return null;
        }

        public void Mate()
        {
            isWaitingForMate = false;
        }

        public virtual void CreateLife(Animal mate)
        {
            Mate();
        }

        public void GetReadyToReceiveMate(IEntity mate)
        {
            isWaitingForMate = true;
            stateMachine.SetWaitingForMate(mate);
        }
    }
}