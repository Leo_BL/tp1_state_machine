using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

namespace Game
{
    /// <summary>
    /// Modifications par Hugues Beaupré-Touchette & Léo Bélanger-Lagacé
    /// </summary>
    public sealed class Bunny : Animal, IPrey
    {
        
        [SerializeField] [Range(0f, 1f)] float nutritiveValue = 1f;

        private BunnyOffspringCreator offspringCreator;
        public bool IsEatable => !Vitals.IsDead;
        
        private new void Awake()
        {
            base.Awake();
            offspringCreator = GetComponentInChildren<BunnyOffspringCreator>();
        }

        public override IPredator GetPredatorInRange()
        {
            var predators = Sensor.For<IPredator>().SensedObjects;

            foreach (IPredator predator in predators)
            {
                return predator;
            }

            return null;
        }

        public override IEatable GetFoodInRange()
        {
            var food = Sensor.For<IVegetable>().SensedObjects;

            foreach (IVegetable vegetable in food)
            {
                if (vegetable.IsEatable)
                    return vegetable;
            }

            return null;
        }

        public override IEntity GetMateInRange()
        {
            var mates = Sensor.For<IPrey>().SensedObjects;

            foreach (IPrey mate in mates)
            {
                if ((mate as Animal).IsDisturbable)
                    return mate;
            }

            return null;
        }

        public IEffect GetEaten()
        {
            Vitals.Die();
            return new LoseHungerEffect(nutritiveValue);
        }
        
        public override void CreateLife(Animal mate)
        {
            base.CreateLife(mate);
            offspringCreator.CreateOffspringWith(mate);
        }
    }
}