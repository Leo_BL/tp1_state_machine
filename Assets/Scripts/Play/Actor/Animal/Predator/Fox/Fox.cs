using System.Linq;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Hugues Beaupré-Touchette & Léo Bélanger-Lagacé
    /// </summary>
    public sealed class Fox : Animal, IPredator
    {
        private FoxOffspringCreator offspringCreator;

        private new void Awake()
        {
            base.Awake();
            offspringCreator = GetComponentInChildren<FoxOffspringCreator>();
        }

        public override IPredator GetPredatorInRange()
        {
            //Fox has no predator.
            return null;
        }

        public override IEatable GetFoodInRange()
        {
            return Sensor.For<IPrey>().SensedObjects.FirstOrDefault(it => it.IsEatable);
        }

        public override IEntity GetMateInRange()
        {
            return Sensor.For<Fox>().SensedObjects.FirstOrDefault(it => it.IsDisturbable);
        }
        
        public override void CreateLife(Animal mate)
        {
            base.CreateLife(mate);
            offspringCreator.CreateOffspringWith(mate);
        }

        
    }
}