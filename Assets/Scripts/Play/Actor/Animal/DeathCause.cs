﻿namespace Game
{
    public enum DeathCause
    {
        THIRST,
        HUNGER,
        EATEN
    }
}