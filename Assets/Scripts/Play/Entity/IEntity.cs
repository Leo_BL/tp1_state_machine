using UnityEngine;

namespace Game
{
    /// <summary>
    /// Modifications par Hugues Beaupré-Touchette
    /// </summary>
    public interface IEntity
    {
        Vector3 Position { get; }
    }
}