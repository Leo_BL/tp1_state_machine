using System;
using System.Data.Common;
using DG.Tweening;
using UnityEngine;
using Random = System.Random;

namespace Game
{
    /// <summary>
    /// Modifications par Léo Bélanger-Lagacé & Hugues Beaupré-Touchette
    /// </summary>
    public class MainController : MonoBehaviour
    {
        [SerializeField] private KeyCode timeScaleUpKey = KeyCode.KeypadPlus;
        [SerializeField] private KeyCode timeScaleDownKey = KeyCode.KeypadMinus;
        [SerializeField] private float timeScaleIncrement = 1;

        private AnimalDeathEventChannel animalDeathEventChannel;
        private AnimalBornEventChannel animalBornEventChannel;
        private StatisticsHandler statisticsHandler;
        private SimulationHandler simulationHandler;

        private DbConnection connection;

        private void Awake()
        {
            DOTween.Init(false, false, LogBehaviour.ErrorsOnly);
            DOTween.SetTweensCapacity(200, 125);
            animalDeathEventChannel = Finder.AnimalDeathEventChannel;
            animalBornEventChannel = Finder.AnimalBornEventChannel;

            simulationHandler = GetComponent<SimulationHandler>();
            statisticsHandler = GetComponent<StatisticsHandler>();
        }

        private void OnEnable()
        {
            animalDeathEventChannel.OnEventPublished += NotifyAnimalDeath;
            animalBornEventChannel.OnEventPublished += NotifyAnimalBorn;
            
        }
        
        private void NotifyAnimalDeath(Animal animal)
        {
            statisticsHandler.IncrementDeath(animal);
        }

        private void NotifyAnimalBorn(Animal animal)
        {
            statisticsHandler.IncrementBirth(animal);
        }
        
        private void Update()
        {
            if (Input.GetKeyDown(timeScaleUpKey))
                Time.timeScale += timeScaleIncrement;
            if (Input.GetKeyDown(timeScaleDownKey))
                Time.timeScale -= timeScaleIncrement;
        }
    }
}