﻿using UnityEngine;

namespace Game
{
    /// <summary>
    /// Léo Bélanger-Lagacé & Hugues Beaupré-Touchette
    /// </summary>
    public delegate void EventChannelEventHandler(Animal animal);
    
    public abstract class AnimalEventChannel : MonoBehaviour
    {
        public event EventChannelEventHandler OnEventPublished;
        
        public void Publish(Animal animal)
        {
            if (OnEventPublished != null)
                OnEventPublished(animal);
        }
    }
}